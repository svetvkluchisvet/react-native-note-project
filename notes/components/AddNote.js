import React from "react";
import {
  Text,
  StyleSheet,
  ScrollView,
  KeyboardAvoidingView,
  TouchableWithoutFeedback,
  Keyboard,
  View,
  TextInput,
  TouchableOpacity,
  Alert,
} from "react-native";

const AddNote = ({ navigation, ...props }) => {
  return (
    <ScrollView>
      <KeyboardAvoidingView
        behavior={Platform.OS === "ios" ? "padding" : "height"}
      >
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
          <View style={{ padding: 20, justifyContent: "space-around" }}>
            <TextInput
              style={[
                { backgroundColor: "lightgreen", borderColor: "lightgreen" },
                styles.input,
              ]}
              placeholder="Type here"
              placeholderTextColor={"black"}
              multiline={true}
              value={props.note}
              onChangeText={(text) => props.setNote(text)}
            />

            <TouchableOpacity
              style={styles.button}
              onPress={() => {
                if (props.note === "") {
                  Alert.alert("Please Type Something");
                } else {
                  props.handleNote();
                  navigation.navigate("Notes");
                }
              }}
            >
              <Text style={styles.buttonText}>Add</Text>
            </TouchableOpacity>
          </View>
        </TouchableWithoutFeedback>
      </KeyboardAvoidingView>
    </ScrollView>
  );
};

export const styles = StyleSheet.create({
  addNoteContainer: {
    paddingHorizontal: 20,
    marginTop: 20,
    alignItems: "center",
    justifyContent: "center",
  },
  input: {
    height: 300,
    paddingTop: 20,
    padding: 20,
    width: "100%",
    fontSize: 19,
    fontWeight: "600",
    color: "black",
    opacity: 0.8,
    elevation: 5,
    borderWidth: 2,
    borderRadius: 10,
  },
  button: {
    backgroundColor: "gray",
    width: "40%",
    borderRadius: 100,
    alignItems: "center",
    justifyContent: "center",
    height: 40,
    alignSelf: "flex-end",
    marginTop: 20,
  },
  buttonText: {
    color: "white",
    fontWeight: "700",
    fontSize: 20,
  },
});
export default AddNote;
